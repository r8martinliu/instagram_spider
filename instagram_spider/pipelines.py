# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


# class InstagramSpiderPipeline(object):
#     def process_item(self, item, spider):
#         return item


import pymongo

class MongoPipeline(object):

    collection_name = 'instagram_posts'

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = 'mongodb://martin2:12345678@localhost:27017/admin'
        self.mongo_db = 'scrapy_insta'

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        self.db[self.collection_name].insert_one(dict(item))
        return item