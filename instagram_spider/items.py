# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


# class InstagramPost(scrapy.Item):
#     # define the fields for your item here like:
#     uid = scrapy.Field()
#     display_url = scrapy.Field()
#     comments = scrapy.Field()
#     shortcode = scrapy.Field()
#     is_video = scrapy.Field()
#     edge_media_to_tagged_user = scrapy.Field()
#     edge_media_to_caption = scrapy.Field()
#     edge_media_to_comment = scrapy.Field()
#     timestamp = scrapy.Field()
#     edge_media_preview_like = scrapy.Field()
#     video_view_count = scrapy.Field()
#     username = scrapy.Field()
#     profile_pic = scrapy.Field()
#     user_id = scrapy.Field()
#     user_fullname = scrapy.Field()
#     is_ad = scrapy.Field()
#     location = scrapy.Field()
#     video_url = scrapy.Field()
#     hashtag = scrapy.Field()

    # pass

class InstagramPost(scrapy.Item):
    shortcode= scrapy.Field()
    uid=scrapy.Field()
    display_url= scrapy.Field()
    is_video= scrapy.Field()
    edge_media_to_tagged_user= scrapy.Field()
    edge_media_to_caption= scrapy.Field()
    edge_media_to_comment= scrapy.Field()
    timestamp= scrapy.Field()
    edge_media_preview_like= scrapy.Field()
    user_fullname= scrapy.Field()
    is_ad= scrapy.Field()
    location= scrapy.Field()
    video_url=scrapy.Field()
    extra_info= scrapy.Field()
    published_date= scrapy.Field()
    platform= scrapy.Field()
    like_count= scrapy.Field()
    comment_count= scrapy.Field()
    view_count= scrapy.Field()
    author_id= scrapy.Field()
    author_name= scrapy.Field()
    author_image_url= scrapy.Field()
    link= scrapy.Field()
    post_data= scrapy.Field()
    title = scrapy.Field()
    post_type = scrapy.Field()


class InstagramComment(scrapy.Item):
    uid = scrapy.Field()
    content = scrapy.Field()
    timestamp = scrapy.Field()
    published_date = scrapy.Field()
    author_id= scrapy.Field()
    author_name= scrapy.Field()
    author_image_url= scrapy.Field()
    like_count= scrapy.Field()
    parentID = scrapy.Field()
    post_type = scrapy.Field()
    extra_info = scrapy.Field()
