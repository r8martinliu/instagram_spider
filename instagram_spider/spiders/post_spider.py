import scrapy
import json
from ..items import *
import urllib
from datetime import datetime
import json
from furl import furl

class HashtagSpider(scrapy.Spider):
    name = "hashtag"


    # Init to take a file as parameter
    def __init__(self, tags):
        with open(tags, 'r') as f:
            self.tags = json.loads(f.read())
            # self.tags = f.read().split()
            self.cookie = {
             # 'sessionid': '2059719584%3AUwCZJVKOjPgYSQ%3A2',
                'sessionid': '2059719584%3Amzfp5IeTiAU2Sv%3A26'
            }

    def start_requests(self):
        for tag in self.tags:
            url = 'https://www.instagram.com/explore/tags/{}/?__a=1'.format(tag['tag'])
            yield scrapy.Request(url=url, meta={'tag': tag}, callback=self.parse_scan_page)



    def parse_scan_page(self, response):
        page = json.loads(response.body_as_unicode())
        shortcodes = [_['node']['shortcode'] for _ in page['graphql']['hashtag']['edge_hashtag_to_media']['edges']]
        post_urls = ['https://www.instagram.com/p/{}?__a=1'.format(_) for _ in shortcodes]

        for post_url in post_urls:
            yield scrapy.Request(url=post_url, meta={'tag': response.meta['tag']}, callback=self.parse_post_url)

        page_info = page['graphql']['hashtag']['edge_hashtag_to_media']['page_info']
        cursor = page_info['end_cursor']
        has_next_page = page_info['has_next_page']
        hashtag = page['graphql']['hashtag']['name']
        if has_next_page:
            queryVariables = '{"tag_name":"' + hashtag + '", "show_ranked":false, "first":12,"after":"' + cursor + '"}'
            next_page_url = 'https://www.instagram.com/graphql/query/?query_hash=ded47faa9a1aaded10161a2ff32abb6b&variables={}'.format(urllib.parse.quote(queryVariables))

            # cookie = {
            #     'sessionid': '2059719584%3AUwCZJVKOjPgYSQ%3A2',
            # }
            yield scrapy.Request(url=next_page_url, cookies=self.cookie, meta={'tag': response.meta['tag']}, callback=self.parse_next_page)


    def parse_next_page(self, response):
        page = json.loads(response.body_as_unicode())
        shortcodes = [_['node']['shortcode'] for _ in page['data']['hashtag']['edge_hashtag_to_media']['edges']]
        post_urls = ['https://www.instagram.com/p/{}?__a=1'.format(_) for _ in shortcodes]

        for post_url in post_urls:
            yield scrapy.Request(url=post_url, meta={'tag': response.meta['tag']}, callback=self.parse_post_url)

        page_info = page['data']['hashtag']['edge_hashtag_to_media']['page_info']
        # print(page)
        cursor = page_info['end_cursor']
        has_next_page = page_info['has_next_page']
        hashtag = page['data']['hashtag']['name']
        # print(hashtag)
        # print(cursor)

        if has_next_page:
            queryVariables = '{"tag_name":"' + hashtag + '", "show_ranked":false, "first":50,"after":"' + cursor + '"}'
            next_page_url = 'https://www.instagram.com/graphql/query/?query_hash=ded47faa9a1aaded10161a2ff32abb6b&variables={}'.format(urllib.parse.quote(queryVariables))

            cookie = {
             'sessionid': '2059719584%3AUwCZJVKOjPgYSQ%3A2',
             }
            yield scrapy.Request(url=next_page_url, cookies=cookie, meta={'tag': response.meta['tag']}, callback=self.parse_next_page)

    def parse_post_url(self, response):
        page = json.loads(response.body_as_unicode())
        info = page['graphql']['shortcode_media']

        if info['is_video']:
            typ = 'video'
            comment_type = 'video_comment'
        else:
            typ = ''
            comment_type = 'comment'

        post_data = info['edge_media_to_caption']['edges'][0]['node']['text']

        post = InstagramPost(
            shortcode = info['shortcode'],
            uid = info['id'],
            display_url = info['display_url'],
            is_video = info['is_video'],
            edge_media_to_tagged_user = info['edge_media_to_tagged_user'],
            edge_media_to_caption = info['edge_media_to_caption'],
            edge_media_to_comment = info['edge_media_to_comment'],
            timestamp = info['taken_at_timestamp'],
            edge_media_preview_like = info['edge_media_preview_like'],
            user_fullname = info['owner']['full_name'],
            is_ad = info['is_ad'],
            location = info['location'],
            video_url = info.get('video_url'),
            extra_info = response.meta['tag'],
            published_date = datetime.fromtimestamp(info['taken_at_timestamp']).strftime('%Y-%m-%d %H:%M:%S'),
            platform = 'instagram',
            like_count = info['edge_media_preview_like']['count'],
            comment_count = info['edge_media_to_comment']['count'],
            view_count=info.get('video_view_count', 0),
            author_id = info['owner']['id'],
            author_name = info['owner']['username'],
            author_image_url = info['owner']['profile_pic_url'],
            link = 'https://www.instagram.com/p/{}'.format(info['shortcode']),
            post_data = post_data,
            title = post_data[:20],
            post_type = typ,
        )
        yield post

        if info['edge_media_to_comment']['count'] > 0:
            for edge in info['edge_media_to_comment']['edges']:
                edge = edge['node']
                yield InstagramComment(
                    uid=edge['id'],
                    content = edge['text'],
                    timestamp = edge['created_at'],
                    published_date = datetime.fromtimestamp(int(edge['created_at'])),
                    author_id = edge['owner']['id'],
                    author_name = edge['owner']['username'],
                    author_image_url = edge['owner']['profile_pic_url'],
                    like_count = edge['edge_liked_by']['count'],
                    parentID = info['id'],
                    post_type = comment_type,
                    extra_info = response.meta['tag'],
                )

        if info['edge_media_to_comment']['page_info']['has_next_page']:
            f = furl('https://www.instagram.com/graphql/query')
            f.args['query_hash'] = 'f0986789a5c5d17c2400faebf16efd0d'
            variables = {
                'shortcode': info['shortcode'],
                'first': 50,
                'after': info['edge_media_to_comment']['page_info']['end_cursor'],
            }
            f.args['variables'] = json.dumps(variables)
            print(f.args['variables'])
            print(f.url)
            original_post_info = {
                'parentID': info['id'],
                'post_type': comment_type,
                'shortcode': info['shortcode'],
            }
            yield scrapy.Request(url=f.url, cookies=self.cookie, meta={'tag': response.meta['tag'], 'original_post_info': original_post_info},
                                 callback=self.parse_comment_url)

    def parse_comment_url(self, response):
        page = json.loads(response.body_as_unicode())
        for edge in page['data']['shortcode_media']['edge_media_to_comment']['edges']:
            edge = edge['node']
            yield InstagramComment(
                uid=edge['id'],
                content=edge['text'],
                timestamp=edge['created_at'],
                published_date=datetime.fromtimestamp(int(edge['created_at'])),
                author_id=edge['owner']['id'],
                author_name=edge['owner']['username'],
                author_image_url=edge['owner']['profile_pic_url'],
                like_count=edge['edge_liked_by']['count'],
                parentID=response.meta['original_post_info']['parentID'],
                post_type=response.meta['original_post_info']['post_type'],
                extra_info=response.meta['tag'],
            )

        if page['data']['shortcode_media']['edge_media_to_comment']['page_info']['has_next_page']:
            f = furl('https://www.instagram.com/graphql/query')
            f.args['query_hash'] = 'f0986789a5c5d17c2400faebf16efd0d'
            variables = {
                'shortcode': response.meta['original_post_info']['shortcode'],
                'first': 24,
                'after': page['data']['shortcode_media']['edge_media_to_comment']['page_info']['end_cursor'],
            }
            f.args['variables'] = json.dumps(variables)

            print(f.url)

            yield scrapy.Request(url=f.url, cookies=self.cookie, meta={'tag': response.meta['tag'], 'original_post_info': response.meta['original_post_info']},
                                 callback=self.parse_comment_url)


    # def hashStr(strInfo):
    #     h = hashlib.md5()
    #     h.update(strInfo.encode("utf-8"))
    #     return h.hexdigest()